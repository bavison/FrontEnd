/* Copyright 1999 Element 14 Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* Title:   FEmem.c
 * Purpose: allocating space in app. space/RMA
 * Author:  IDJ
 * History: 18-Apr-90: IDJ: created
 *          05-Mar-91: IDJ: use msgs.h
 *
 *          Re-release started (Oct-91)
 *          04-Nov-91: IDJ: removed unused functions FEmem_RMAalloc/free
 *          04-Noc-91: IDJ: added FEmem_realloc
 *
 */

#include "kernel.h"
#include "swis.h"
#include <stdlib.h>

#include "bool.h"
#include "werr.h"
#include "msgs.h"
#include "FEinterr.h"

#include "FEmem.h"

/* currently (24-Apr-90) all memory comes from the C heap
 * when processor is in USR mode this means from the wimpslot,
 * when in SVC mode from the RMA
 */


void *FEmem_alloc(unsigned int size)
{
   void *res;

   if ((res = malloc(size)) == 0)
      werr(TRUE, msgs_lookup("FEmem1:Out of memory, on request for %d bytes"), size);

   return res;
}

void FEmem_free(void *p)
{
   free(p);
}

void *FEmem_realloc(void *p, unsigned int size)
{
   void *res;

   if ((res = realloc(p, size)) == 0)
      werr(TRUE, msgs_lookup("FEmem1:Out of memory, on request for %d bytes"), size);
   return res;
}

